package keycloak

import (
	"context"
	"github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"net/http"
)

type ClientAuth struct {
	ClientID     string
	ClientSecret string
}

func NewOAuthClient(ctx context.Context, provider *oidc.Provider, auth *ClientAuth, scopes []string) (client *http.Client) {
	endpoint := provider.Endpoint()
	config := &clientcredentials.Config{
		ClientID:     auth.ClientID,
		ClientSecret: auth.ClientSecret,
		TokenURL:     endpoint.TokenURL,
		Scopes:       scopes,
		AuthStyle:    oauth2.AuthStyleInHeader,
	}

	return config.Client(ctx)
}
