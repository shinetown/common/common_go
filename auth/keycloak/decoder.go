package keycloak

import (
	"context"
	"encoding/json"
	"github.com/coreos/go-oidc"
	"time"
)

func NewTokenDecoder(keySet oidc.KeySet) *TokenDecoder {
	return &TokenDecoder{
		KeySet: keySet,
	}
}

type TokenDecoder struct {
	KeySet oidc.KeySet
}

func (m *TokenDecoder) DecodeToken(ctx context.Context, token string) (t *Token, err error) {
	payload, err := m.KeySet.VerifySignature(ctx, token)
	if err != nil {
		return
	}
	t = new(Token)
	if err = json.Unmarshal(payload, &t); err != nil {
		return nil, err
	}

	if t.ExpiresAt != 0 && time.Unix(t.ExpiresAt, 0).Before(time.Now()) {
		return nil, ErrTokenExpiry
	}

	return
}
