package keycloak

import "errors"

var (
	ErrTokenExpiry = errors.New("auth: token is expiry")
)

type ResponseError struct {
	ErrorCode        string `json:"error"`
	ErrorDescription string `json:"error_description,omitempty"`
}

func (e *ResponseError) Error() string {
	return e.ErrorCode + " " + e.ErrorDescription
}
