package keycloak

import (
	"context"
	"github.com/coreos/go-oidc"
	"os"
)

func NewProviderFromEnv() (*oidc.Provider, error) {
	return oidc.NewProvider(context.Background(), os.Getenv("OIDC_ISSUER"))
}

func NewKeySet(ctx context.Context, provider *oidc.Provider) (oidc.KeySet, error) {
	var keySetClaim struct {
		JwksUri string `json:"jwks_uri"`
	}
	if err := provider.Claims(&keySetClaim); err != nil {
		return nil, err
	}

	return oidc.NewRemoteKeySet(ctx, keySetClaim.JwksUri), nil
}
