package keycloak

type Token struct {
	ExpiresAt   int64  `json:"exp,omitempty"`
	Id          string `json:"jti,omitempty"`
	IssuedAt    int64  `json:"iat,omitempty"`
	Issuer      string `json:"iss,omitempty"`
	NotBefore   int64  `json:"nbf,omitempty"`
	Subject     string `json:"sub,omitempty"`
	Email       string `json:"email"`
	RealmAccess struct {
		Roles Roles `json:"roles,omitempty"`
	} `json:"realm_access,omitempty"`
}

type Roles []string

func (roles Roles) Contains(role string) bool {
	for _, r := range roles {
		if r == role {
			return true
		}
	}
	return false
}

type TokenStatus struct {
	Active    bool   `json:"active"`
	Scope     string `json:"scope,omitempty"`
	ClientId  string `json:"client_id,omitempty"`
	Username  string `json:"username,omitempty"`
	TokenType string `json:"token_type,omitempty"`
	Expiry    int64  `json:"exp,omitempty"`
	IssueAt   int64  `json:"iat,omitempty"`
	NotBefore int64  `json:"nbf,omitempty"`
	Subject   string `json:"sub,omitempty"`
	Audience  string `json:"aud,omitempty"`
	Issuer    string `json:"iss,omitempty"`
	JwtId     string `json:"jti,omitempty"`
}
