package keycloak

import (
	"context"
	"encoding/json"
	"github.com/coreos/go-oidc"
	"net/http"
	"net/url"
	"strings"
)

func NewTokenIntrospect(provider *oidc.Provider, client *http.Client) *TokenIntrospect {
	return &TokenIntrospect{
		Provider: provider,
		Client:   client,
	}
}

type TokenIntrospect struct {
	Provider *oidc.Provider
	Client   *http.Client
}

func (m *TokenIntrospect) Introspect(ctx context.Context, token string) (status *TokenStatus, err error) {
	form := url.Values{}
	form.Set("token", token)
	form.Set("token_type_hint", "access_token")
	var claim struct {
		TokenIntrospectionEndpoint string `json:"token_introspection_endpoint"`
	}
	if err = m.Provider.Claims(&claim); err != nil {
		return
	}

	req, err := http.NewRequest(http.MethodPost, claim.TokenIntrospectionEndpoint, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, err := m.Client.Do(req.WithContext(ctx))
	if err != nil {
		return
	}
	defer func() {
		_ = res.Body.Close()
	}()

	if res.StatusCode != http.StatusOK {
		var errorResponse ResponseError
		if err = json.NewDecoder(res.Body).Decode(&errorResponse); err != nil {
			return
		}
		return nil, &errorResponse
	}

	status = new(TokenStatus)
	if err = json.NewDecoder(res.Body).Decode(status); err != nil {
		return nil, err
	}

	return
}
