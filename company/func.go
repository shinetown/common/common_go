package company

func Register(region string, info Info) {
	defaultRegistry.Register(region, info)
}

func Get(region string) *Info {
	return defaultRegistry.Get(region)
}
