package company

func init() {
	defaultRegistry.Register("HK", Info{
		Name:    "Shinetown Telecommunincation Limited",
		Address: "Unit 1 - 16, 20/F, Corporation Park, 11 On Lai Streeet, Shatin, Hong Kong",
		Tel:     "+852 2172 7777",
		Fax:     "+852 2172 7778",
	})
	defaultRegistry.Register("SG", Info{
		Name:    "Shinetown Telecom (S) Pte Ltd",
		Address: "140 Paya Lebar Road, #05-06 AZ@Paya Lebar, Singapore 409015",
		Tel:     "(65) 6838 1211",
		Fax:     "(65) 6887 4556",
		Extra:   "company / GST reg. no. : 200002100E",
	})
	defaultRegistry.Register("MY", Info{
		Name:    "SHINETOWN TELECOM SDN BHD",
		Address: "18-01, Jalan LGSB 1/4, Pusat Komersial LGSB, Off Jalan Hospital, 47000 Sungai Buloh, Selangor, Malaysia",
		Tel:     "+603 6141 0038",
		Fax:     "N/A",
		Extra:   "Company Reg. No: 1280533-T",
	})
	defaultRegistry.Register("TW", Info{
		Name:    "台灣信京電訊股份有限公司",
		Address: "台北市士林區中山北路6段41-1號",
		Tel:     "02-2688-5333",
		Fax:     "N/A",
		Extra:   "統一編號：28460932",
	})
}
