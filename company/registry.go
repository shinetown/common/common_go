package company

import "sync"

var defaultRegistry = &registry{
	data: make(map[string]Info),
}

type registry struct {
	data  map[string]Info
	rmMux sync.RWMutex
}

func (r *registry) Register(region string, info Info) {
	r.rmMux.Lock()
	defer r.rmMux.Unlock()
	r.data[region] = info
}

func (r *registry) Get(region string) *Info {
	r.rmMux.RLock()
	defer r.rmMux.RUnlock()
	info, ok := r.data[region]
	if !ok {
		return nil
	}
	return &info
}
