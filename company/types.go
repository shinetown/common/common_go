package company

type Info struct {
	Name    string
	Address string
	Tel     string
	Fax     string
	Extra   string
}
