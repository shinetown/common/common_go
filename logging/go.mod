module gitlab.com/shinetown/common/common_go/logging

go 1.14

require (
	cloud.google.com/go v0.53.0 // indirect
	github.com/fluent/fluent-logger-golang v1.5.0 // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/google/wire v0.4.0
	github.com/knq/sdhook v0.0.0-20190801142816-0b7fa827d09a
	github.com/sirupsen/logrus v1.4.2
	github.com/tinylib/msgp v1.1.1 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	google.golang.org/api v0.19.0
	google.golang.org/genproto v0.0.0-20200226201735-46b91f19d98c // indirect
)
