package logging

import (
	"github.com/google/wire"
	"github.com/sirupsen/logrus"
)

var (
	RootLogger     = logrus.New()
	WireRootLogger = wire.Value(RootLogger)
)
