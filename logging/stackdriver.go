package logging

import (
	"github.com/knq/sdhook"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/logging/v2"
	"os"
)

// NewStackdriverHook create logrus hook for connecting to stackdriver
func NewStackdriverHook() (logrus.Hook, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	credentialFile := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	return sdhook.New(
		sdhook.GoogleServiceAccountCredentialsFile(credentialFile),
		sdhook.MonitoredResource(&logging.MonitoredResource{
			Type: "container",
			Labels: map[string]string{
				"pod_id":         hostname,
				"container_name": os.Getenv("K8S_CONTAINER_NAME"),
			},
		}),
	)
}
