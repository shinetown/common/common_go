package exporter

import (
	"contrib.go.opencensus.io/exporter/prometheus"
	prom "github.com/prometheus/client_golang/prometheus"
	"go.opencensus.io/stats/view"
)

func NewPrometheusExporter(namespace string) (pe *prometheus.Exporter) {
	pe, _ = prometheus.NewExporter(prometheus.Options{
		Namespace: namespace,
		Registry:  prom.DefaultRegisterer.(*prom.Registry),
	})
	view.RegisterExporter(pe)
	return
}
