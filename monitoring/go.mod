module gitlab.com/shinetown/common/common_go/monitoring

go 1.14

require (
	contrib.go.opencensus.io/exporter/prometheus v0.1.0
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/prometheus/client_golang v1.4.1
	github.com/prometheus/procfs v0.0.10 // indirect
	go.opencensus.io v0.22.3
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
)
