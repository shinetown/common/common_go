package monitoringhttp

import (
	"go.opencensus.io/plugin/ochttp"
	"net/http"
)

func WrapTransport(base http.RoundTripper) *ochttp.Transport {
	if base == nil {
		base = http.DefaultTransport
	}
	return &ochttp.Transport{
		Base: base,
	}
}

func WrapClient(client *http.Client) *http.Client {
	if client == nil {
		client = http.DefaultClient
	}
	return &http.Client{
		Transport: WrapTransport(client.Transport),
	}
}
