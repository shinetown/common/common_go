package monitoringhttp

import (
	"go.opencensus.io/plugin/ochttp"
	"net/http"
)

func WrapHandler(handler http.Handler) *ochttp.Handler {
	return &ochttp.Handler{
		Handler: handler,
	}
}
