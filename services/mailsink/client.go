package mailsink

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
	"sync"
)

type Client struct {
	Hostname   string
	Client     *http.Client
	BufferPool *sync.Pool
	Logger     logrus.FieldLogger
}

func (mc *Client) SendMail(ctx context.Context, mail *Mail) (err error) {
	uri := strings.TrimSuffix(mc.Hostname, "/") + "/v1/send"

	buffer := mc.BufferPool.Get().(*bytes.Buffer)
	defer mc.BufferPool.Put(buffer)
	buffer.Reset()

	if err = json.NewEncoder(buffer).Encode(mail); err != nil {
		mc.Logger.Error(err)
		return
	}
	req, err := http.NewRequest(http.MethodPost, uri, buffer)
	if err != nil {
		mc.Logger.Error(err)
		return
	}

	res, err := mc.Client.Do(req.WithContext(ctx))
	if err != nil {
		mc.Logger.Error(err)
		return
	}
	defer func() {
		_ = res.Body.Close()
	}()

	if res.StatusCode != http.StatusOK {
		return &UnexpectedResponseStatusError{Status: res.StatusCode}
	}

	return
}
