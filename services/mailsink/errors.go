package mailsink

import (
	"errors"
	"fmt"
)

var ErrUnexpectedResponseStatus = errors.New("unexpected response status")

type UnexpectedResponseStatusError struct {
	Status int
}

func (e UnexpectedResponseStatusError) Error() string {
	return fmt.Sprintf("unexpected response status receive: %d", e.Status)
}

func (e UnexpectedResponseStatusError) Unwrap() error {
	return ErrUnexpectedResponseStatus
}
