package mailsink

type Mail struct {
	From        string       `json:"from"`
	FromName    string       `json:"fromName,omitempty"`
	To          string       `json:"to"`
	Subject     string       `json:"subject"`
	CC          []string     `json:"cc,omitempty"`
	BCC         []string     `json:"bcc,omitempty"`
	ReplyTo     string       `json:"replyTo,omitempty"`
	Message     string       `json:"message"`
	Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Url    string `json:"url"`
	Type   string `json:"type"`
	Name   string `json:"name"`
	Inline bool   `json:"inline"`
}
